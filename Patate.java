/**
 * Patate
 */

import java.awt.Graphics2D;
import java.awt.event.KeyEvent;

public class Patate {
  
  public Patate(Game game) {
    this.game = game;
  }

  //variable assign
  private Game game;
  double size = 30.0;
  double xSpeed = 0.0;
  double ySpeed = 0.0;
  double x = 0.0;
  double y = 0.0;
  double fric = 0.05;
  boolean init = false;
  String dir = "right";
  double maxSpeed = 3.0;

  public void move() {
    if (init == false) {
      y = game.getHeight() - size;
      init = true;
    }
    boundaries();
    x += xSpeed;
    y += ySpeed;
    System.out.println(dir);
    System.out.println(xSpeed);
    if (xSpeed > 0.01) {
      xSpeed -= fric;
    }
    if (xSpeed < -0.01) {
      xSpeed += fric;
    }
    maxSpeedFunc();
  }

  private void boundaries() {
    if ((x + xSpeed < 0) || x + xSpeed > game.getWidth() - size) {
      xSpeed = -xSpeed;
    }
    if ((y + ySpeed < 0) || y + ySpeed > game.getHeight() - size) {
      ySpeed = -ySpeed;
    }
  }

  private void maxSpeedFunc() {
    if (xSpeed > maxSpeed) {
      xSpeed = maxSpeed;
    }
    if (xSpeed < -maxSpeed) {
      xSpeed = -maxSpeed;
    }
  }

  public void paint(Graphics2D g) {
		g.fillOval((int)x, (int)y, 30, 30);
  }

  public void keyReleased(KeyEvent e) {
  }

	public void keyPressed(KeyEvent e) {
    fric = 0.0;
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
      xSpeed -= 1.0;
      dir = "left";
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
      xSpeed += 1.0;
      dir = "right";
	}

}